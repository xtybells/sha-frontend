let app = new Vue({
  el: "#app",
  data: {
    cohortUrl: "http://localhost:5000/read-cohorts",
    locationUrl: "http://localhost:5000/location/read-locations",
    participantUrl: "http://localhost:5000/participant/read-participants",
    locations: [],
    participants: [],
    selectedLocation: "",
    cohortCode: "",
    cohortStartDate: "",
    cohortEndDate: "",
    selectedState: "",
    cohortSnackBar: false,
    snackbarMsg: "No message",
    isError: false,
    messageColor: "",

    // add participant data
    participantId: "",
    ParticipantFirstName: "",
    ParticipantLastName: "",
    ParticipantGender: "",
    paticipantcohortCode: "",
    participantStatus: "",
    participantLinkedIn: "",
    participantGithub: "",
    participantEmail: "",
    participantPhoneNumber: "",
    comment: "",
    id: "",
    isEditing: false,

    //view cohort
    searchBar: "",
    searchResults: [],
    participantsBackup: [],

    //index
    numberOfParticipants: "",
    numberOfFemale: "",
    numberOfMale: ""
  },
  methods: {
    postCohortData: async function() {
      event.preventDefault();
      let res = await fetch("http://localhost:5000/cohort/add-cohort", {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          code: this.cohortCode,
          startDate: this.cohortStartDate,
          endDate: this.cohortEndDate,
          country: this.selectedLocation.country,
          state: this.selectedState
        })
      });
      if (!res) {
        // console.log("no result");
        alert("No response from server!");
        return;
      }
      let result = await res.json();
      // console.log("result", result);

      this.snackbarMsg = result.message;
      if (result.error === true) {
        this.messageColor = "errorMsg";
      } else {
        this.messageColor = "successMsg";
      }

      this.cohortSnackBar = true;
      setTimeout(() => {
        this.cohortSnackBar = false;
      }, 3000);
    },
    fetchParticipantData: async function() {
      let res = await fetch(this.participantUrl, {
        method: "GET"
      });
      if (!res) {
        // console.log("no participant in cohort");
        return;
      }
      this.participants = await res.json();
      // console.log(this.participants);
      this.participantsBackup = [...this.participants];

      //total of participants
      for (let i = 0; i < this.participants.length; i++) {}
      this.numberOfParticipants = this.participants.length;

      for(let x=0; x<this.participantsBackup.length; x++){
        let allGender = this.participantsBackup[x].gender
        if(allGender === "Male"){
          this.numberOfMale = allGender.length
        }else if(allGender === "Female"){
          this.numberOfFemale = allGender.length
        }
      }

   
    },
    fetchLocationData: async function() {
      let res = await fetch(this.locationUrl, {
        method: "GET"
      });
      if (!res) {
        // console.log("no result");
        return;
      }
      this.locations = await res.json();
      // console.log(this.locations);
    },

    postParticipantData: async function() {
      this.fetchLocationData();
      event.preventDefault();
      let res = await fetch(
        "http://localhost:5000/participant/add-participant",
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json"
          },
          body: JSON.stringify({
            firstName: this.ParticipantFirstName,
            lastName: this.ParticipantLastName,
            gender: this.ParticipantGender,
            cohort: this.paticipantcohortCode,
            status: this.participantStatus,
            linkedin: this.participantLinkedIn,
            country: this.selectedLocation.country,
            state: this.selectedState,
            github: this.participantGithub,
            email: this.participantEmail,
            phoneNumber: this.participantPhoneNumber,
            comment: this.comment
          })
        }
      );
      if (!res) {
        console.log("no result");
        alert("No response from server!");
        return;
      }
      let result = await res.json();
      // console.log("result", result);

      this.snackbarMsg = result.message;
      if (result.error === true) {
        this.messageColor = "errorMsg";
      } else {
        this.messageColor = "successMsg";
      }

      this.cohortSnackBar = true;
      setTimeout(() => {
        this.cohortSnackBar = false;
      }, 3000);
    },

    allSearch: function() {
      this.searchBar = this.searchBar.trim().toLowerCase();
      this.searchResults = [];
      this.participants = [...this.participantsBackup];
      for (let z = 0; z < this.participants.length; z++) {
        if (!this.participants[z].state) {
          this.participants[z].state = "";
        }
        if (
          this.searchBar === this.participants[z].firstName.trim().toLowerCase()
        ) {
          this.searchResults.push(this.participants[z]);
          // console.log(this.searchResults);
        } else if (
          this.searchBar === this.participants[z].lastName.trim().toLowerCase()
        ) {
          this.searchResults.push(this.participants[z]);
          // console.log(this.searchResults);
        } else if (
          this.searchBar === this.participants[z].gender.trim().toLowerCase()
        ) {
          this.searchResults.push(this.participants[z]);
          // console.log(this.searchResults);
        } else if (this.searchBar === this.participants[z].cohort) {
          this.searchResults.push(this.participants[z]);
          // console.log(this.searchResults);
        } else if (
          this.searchBar === this.participants[z].state.trim().toLowerCase()
        ) {
          this.searchResults.push(this.participants[z]);
          // console.log(this.searchResults);
        } else if (
          this.searchBar ===
          this.participants[z].phoneNumber.trim().toLowerCase()
        ) {
          this.searchResults.push(this.participants[z]);
          // console.log(this.searchResults);
        } else if (
          this.searchBar === this.participants[z].email.trim().toLowerCase()
        ) {
          this.searchResults.push(this.participants[z]);
          // console.log(this.searchResults);
        }
      }
      this.participants = this.searchResults;
    },

    deleteParticipant: async function(participant) {
      if (
        confirm("Are you sure you want to delete this participant?") === false
      ) {
        return;
      }
      let res = await fetch(
        "http://localhost:5000/participant/delete-participant" +
          "/" +
          participant._id,
        {
          method: "DELETE"
        }
      );
      if (!res) {
        // console.log(" cannot delete ");
        return;
      } else {
        this.fetchParticipantData();
        // console.log("sucessfuly deleted");
      }
    },

  

    initAddParticipantPage: function(){
      const capturedParticipantString = localStorage.getItem("participant")
      localStorage.removeItem("participant")
      let capturedParticipant = {}
      if(capturedParticipantString) {
        this.isEditing = true;
        capturedParticipant = JSON.parse(capturedParticipantString)
        this.ParticipantFirstName = capturedParticipant.firstName;
        this.ParticipantLastName = capturedParticipant.lastName;
        this.ParticipantGender = capturedParticipant.gender;
        this.paticipantcohortCode = capturedParticipant.cohort;
        this.participantEmail = capturedParticipant.email;
        this.participantGithub = capturedParticipant.github;
        this.participantLinkedIn = capturedParticipant.linkedin;
        this.participantPhoneNumber = capturedParticipant.phoneNumber;
        this.participantStatus = capturedParticipant.status;
        this.selectedLocation = capturedParticipant.country;
        this.selectedState = capturedParticipant.state;
        this.comment = capturedParticipant.comment
        this.participantId = capturedParticipant._id
      }
    },

    captureParticipant: function(participant){
      // console.log(participant)
      localStorage.setItem('participant', JSON.stringify(participant));
      window.location.href = "/pages/add-participant.html"
    },

    updateParticipant: async function(){
      let res = await fetch(`http://localhost:5000/participant/update-participant/${this.participantId}`, {
        method: "PUT",
        headers:{
          "Content-type": "application/json"
        },
        body: JSON.stringify({
          firstName: this.ParticipantFirstName,
          lastName:this.ParticipantLastName,
          gender:this.ParticipantGender,
          status:this.participantStatus,
          linkedin:this.participantLinkedIn,
          github:this.participantGithub,
          email:this.participantEmail,
          phoneNumber:this.participantPhoneNumber,
          cohort: this.paticipantcohortCode,
          country:this.selectedLocation.country,
          state:this.selectedState
          
        })
      })
      let updatedResult = await res.json()
      if(!res){
        alert("no response")
        return
      }else{
        console.log(updatedResult)
      }

      this.snackbarMsg = updatedResult.message;
      if (updatedResult.error === true) {
        this.messageColor = "errorMsg";
      } else {
        this.messageColor = "successMsg";
      }

      this.cohortSnackBar = true;
      setTimeout(() => {
        this.cohortSnackBar = false;
      }, 3000);
  }
  
  },
  mounted: function() {
    this.fetchLocationData();
    this.fetchParticipantData();
    this.initAddParticipantPage();
  }
});
